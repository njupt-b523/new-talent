# Ubuntu18.04_env

#### 介绍
The entire process of configuring the Ubuntu18.04 software environment

#### 软件目录
Ubuntu18.04; ROS-melodic; Opencv3.3.1; Ceres; Eigen3 .etc

#### Ubuntu18.04
1.    安装双系统需要关闭security mode
2.    鼠标右击没有反应
```
gsettings set org.gnome.desktop.peripherals.touchpad click-method areas
```
3.    安裝Ubuntu18.04系統需要在UEFI模式下，分區只需要swap,/,/home和UEFI即可，UEFI給300M左右，用於保存boot。

#### Raspberry Pi 
1.    忘記密碼
```
sudo passwd root

sudo passwd --unlock root
su

passwd XXX

sudo passwd --lock root 
```


#### ROS-Melodic
1.  添加清华源

```
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak

sudo gedit /etc/apt/sources.list

    deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse
    deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse
    deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse
    deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse
    deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse
    deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse
    deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse
    deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse
    deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse
    deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse

sudo apt-get update
```

2.  安装ros-melodic
```
sudo sh -c '. /etc/lsb-release && echo "deb http://mirrors.ustc.edu.cn/ros/ubuntu/ $DISTRIB_CODENAME main" > /etc/apt/sources.list.d/ros-latest.list'

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F42ED6FBAB17C654

sudo apt-get update

sudo apt-get install ros-melodic-desktop-full 

sudo apt-get install ros-melodic-rqt* ros-melodic-usb-camera ros-melodic-rviz* ros-melodic-calibration* ros-melodic-serial

sudo rosdep init 

打开网址：https://site.ip138.com

输入raw.githubusercontent.com

rosdep update

echo ''source /opt/ros/melodic/setup.bash'' >> ~/.bashrc

source ~/.bashrc
```

#### OpenCV 3.3.1
```
sudo apt-get install build-essential

sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev

sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libdc1394-22-dev

cd ~/opencv3.3.1/build

cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local ..

make -j4 

sudo make install

```

#### Ceres
1. delete .git
2. glog
```
git clone https://gitee.com/Bryan_Jiang/glog.git

sudo apt-get install autoconf automake libtool

cmake ..

make -j4

sudo make install
```
3. ceres
```
cmake ..

make -j4

sudo make install
```

#### Pangolin DBoW2 g2o
```
https://gitee.com/Bryan_Jiang/Pangolin.git

https://gitee.com/Bryan_Jiang/DBoW2.git

https://gitee.com/Bryan_Jiang/g2o.git

sudo apt-get install libglew-dev
```


#### Eigen3
```
sudo apt-get install libeigen3-dev
```

#### VINS-Mono
```
git clone https://gitee.com/Bryan_Jiang/VINS-Mono.git

catkin_make
```

#### USB-CAM
```
sudo apt-get install ros-melodic-usb-cam

roslaunch usb_cam usb_cam-test.launch

/opt/ros/melodic/share/usb-cam/launch/
```

#### anaconda
```
conda create -n universe python3.6 anaconda
conda activate universe
conda desactivate
```

#### gym
```
sudo apt-get install swig
pip install gym box2d box2d-kengz
```

#### pycharm or clion desktop icon
```
sudo  gedit  /usr/share/applications/Pycharm.desktop

    [Desktop Entry]
    Type=Application
    Name=Pycharm
    GenericName=Pycharm3
    Comment=Pycharm3:The Python IDE
    Exec=bash -i -c "/home/jiangbin/pycharm-professional-2020.2.3/pycharm-2020.2.3/bin/pycharm.sh" %f
    Icon=/home/jiangbin/CLion-2020.2.4/clion-2020.2.4/bin/pycharm.png
    Terminal=pycharm
    Categories=Pycharm;
```

#### 双目模式切换
```
sudo apt-get install uvcdynctrl

uvcdynctrl -d /dev/video0 -S 6:8  '(LE)0x50ff'
uvcdynctrl -d /dev/video0 -S 6:15 '(LE)0x00f6'
uvcdynctrl -d /dev/video0 -S 6:8  '(LE)0x2500'
uvcdynctrl -d /dev/video0 -S 6:8  '(LE)0x5ffe'
uvcdynctrl -d /dev/video0 -S 6:15 '(LE)0x0003'
uvcdynctrl -d /dev/video0 -S 6:15 '(LE)0x0002'
uvcdynctrl -d /dev/video0 -S 6:15 '(LE)0x0012'
uvcdynctrl -d /dev/video0 -S 6:15 '(LE)0x0004'
uvcdynctrl -d /dev/video0 -S 6:8  '(LE)0x76c3'
uvcdynctrl -d /dev/video0 -S 6:10 '(LE)0x0400'

```

#### GASS
1. GAAS
```
sudo apt install python-catkin-tools ros-melodic-mavlink ros-melodic-geographic-msgs ros-melodic-octomap-ros libgeographic-dev geographiclib-tools ros-melodic-control-toolbox libpopt-dev

git clone https://gitee.com/Bryan_Jiang/mavros.git

git clone https://gitee.com/Bryan_Jiang/octomap_mapping.git

catkin_make

```
2. Gazebo
```
sudo apt install ros-melodic-gazebo-*

```
3. PCL
```
https://pointclouds.org/documentation/tutorials/compiling_pcl_posix.html#compiling-pcl-posix

```
4.  YGZ-slam
```
git clone https://gitee.com/Bryan_Jiang/ygz-stereo-inertial.git

need g2o pangolin fast DBow2

```
5. PX4
```
mkdir ~/px4 && cd ~/px4

git clone https://gitee.com/Bryan_Jiang/Firmware.git

cd Firmware

git checkout v1.8.0

```

6. ROS2
```
sudo apt update && sudo apt install curl gnupg2 lsb-release
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] http://packages.ros.org/ros2/ubuntu bionic main" > /etc/apt/sources.list.d/ros2-latest.list'


curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc > ros.asc
sudo apt-key add ros.asc

sudo apt update
sudo apt install ros-eloquent-desktop
sudo apt install -y libpython3-dev python3-pip
pip3 install -U argcomplete
echo "source /opt/ros/eloquent/setup.bash" >> ~/.bashrc
source ~/.bashrc

若希望ROS和ROS2共存
sudo gedit ~/.bashrc
echo "ros melodic(1) or ros2 eloquent(2)?"
read edition
if [ "$edition" -eq "1" ];then
  source /opt/ros/melodic/setup.bash
else
  source /opt/ros/eloquent/setup.bash
fi
source ~/.bashrc


sudo apt update && sudo apt install -y \
build-essential \
cmake \
git \
python3-colcon-common-extensions \
python3-pip \
python-rosdep \
python3-vcstool \
wget
# install some pip packages needed for testing
sudo -H python3 -m pip install -U \
argcomplete \
flake8 \
flake8-blind-except \
flake8-builtins \
flake8-class-newline \
flake8-comprehensions \
flake8-deprecated \
flake8-docstrings \
flake8-import-order \
flake8-quotes \
pytest-repeat \
pytest-rerunfailures
# [Ubuntu 16.04] install extra packages not available or recent enough on Xenial
python3 -m pip install -U \
pytest \
pytest-cov \
pytest-runner \
setuptools
# install Fast-RTPS dependencies
sudo apt install --no-install-recommends -y \
libasio-dev \
libtinyxml2-dev
```
