### 图像处理：语义分割、实例分割、目标检测
1. Fast R-CNN (目标检测)\
[Paper](https://arxiv.org/abs/1504.08083) | [Code](https://github.com/rbgirshick/fast-rcnn)

2. Mask R-CNN（实例分割+目标检测）\
[Paper](http://cn.arxiv.org/pdf/1703.06870v3) | [Code](https://github.com/facebookresearch/detectron2)

3. BiSeNet: Bilateral Segmentation Network for Real-time Semantic Segmentation (BiSeNet:用于实时语义分割的双边分割网络)\
[Paper](https://arxiv.org/abs/1808.00897) | [Code](https://github.com/CoinCheung/BiSeNet)



### 点云处理：基于深度学习的三维点云处理
1. The Point Cloud Library (PCL)\
[website](https://pointclouds.org/)

2. 深蓝学院三维点云处理课程 (无需购买，有资源)\
[website](https://www.shenlanxueyuan.com/course/313)

3. PointNet: Deep Learning on Point Sets for 3D Classification and Segmentation （ PointNet:用于三维分类和分割的点集深度学习） \
[Paper](https://arxiv.org/abs/1612.00593) | [Code](https://github.com/charlesq34/pointnet.git)
 
4. PointNet++:Deep Hierarchical Feature Learning on Point Sets in a Metric Space (PointNet++:度量空间中点集的深度层次特征学习)\
[Paper](https://arxiv.org/abs/1706.02413) | [Code](https://github.com/charlesq34/pointnet2)

5. VoxelNet: End-to-End Learning for Point Cloud Based 3D Object Detection （VoxelNet:基于点云的三维物体检测的端到端学习）\
[Paper](https://arxiv.org/pdf/1711.06396) | [Code](https://github.com/qianguih/voxelnet)

6. Can GCNs Go as Deep as CNNs? （GCNs能像CNNs一样深入吗?）\
[Paper](https://arxiv.org/abs/1904.03751) | [Code](https://github.com/lightaime/deep_gcns)

7. Dynamic Graph CNN for Learning on Point Clouds (动态图卷积) \
[Paper](https://arxiv.org/abs/1801.07829) | [Code](https://github.com/WangYueFt/dgcnn.git)

8. RandLA-Net: Efficient Semantic Segmentation of Large-Scale Point Clouds（RandLA-Net:高效地大场景点云语义分割）\
[Paper](https://arxiv.org/pdf/1911.11236.pdf) | [Code](https://github.com/QingyongHu/RandLA-Net)

9. Towards Semantic Segmentation of Urban-Scale 3D Point Clouds: A Dataset, Benchmarks and Challenges (走向城市规模3D点云的语义分割：数据集，基准和挑战)\
[Paper](https://arxiv.org/abs/2009.03137) | [Code](https://github.com/QingyongHu/SensatUrban)
 

### 点云配准：基于深度学习的点云配准
1. PointNetLK: Robust & Efficient Point Cloud Registration using PointNet（PointNetLK:使用PointNet进行鲁棒高效的点云配准） \
[Paper](https://arxiv.org/abs/1903.05711) | [Code](https://github.com/hmgoforth/PointNetLK.git)

2. Deep Closest Point Learning Representations for Point Cloud Registration （用于点云配准的深度最近点学习表示）\
[Paper](https://arxiv.org/abs/1905.03304) | [Code](https://github.com/WangYueFt/dcp.git)

3. DeepICP: An End-to-End Deep Neural Network for 3D Point Cloud Registration （DeepICP:用于三维点云配准的端到端深度神经网络）\
[Paper](https://arxiv.org/abs/1905.04153) 

4. PCRNet Point Cloud Registration Network using PointNet Encoding（PCRNet点云配准网络使用点网编码） \
[Paper](https://arxiv.org/abs/1908.07906) | [Code](https://github.com/vinits5/pcrnet.git)

5. A comprehensive survey on point cloud registration（点云配准的综合研究）\
[Paper](https://arxiv.org/abs/2103.02690)

6. PREDATOR: Registration of 3D Point Clouds with Low Overlap (PREDATOR:低重叠三维点云的配准)\
[Paper](https://arxiv.org/abs/2011.13005) | [Code](https://github.com/ShengyuH/OverlapPredator)



### 多传感器融合：基于激光雷达、相机和惯导的多传感器融合
1. VINS-Mono: A Robust and Versatile Monocular Visual-Inertial State Estimator（VINS-Mono：一种鲁棒且通用的单目视觉惯性状态估计器）\
[Paper](https://ieeexplore.ieee.org/document/8421746) | [Code](https://github.com/HKUST-Aerial-Robotics/VINS-Mono)

2. ORB-SLAM3: An Accurate Open-Source Library for Visual, Visual-Inertial and Multi-Map SLAM（ORB-SLAM3:一个精确的可视化、视觉惯性和多地图SLAM的开源库）\
[Paper](https://arxiv.org/pdf/2007.11898.pdf) | [Code](https://github.com/UZ-SLAMLab/ORB_SLAM3)

3. LIC-Fusion2.0: LiDAR-Inertial-Camera Odometry with Sliding-Window Plane-Feature Tracking (LIC-Fusion2.0:滑动窗口平面特征跟踪的激光惯性相机测程法)\
[Paper](https://arxiv.org/pdf/2008.07196.pdf)

4. LIO-SAM: Tightly-coupled Lidar Inertial Odometry via Smoothing and Mapping (LIO-SAM:通过平滑和映射的紧耦合激光雷达惯性里程测量)\
[Paper](https://arxiv.org/abs/2007.00258) | [Code](https://github.com/TixiaoShan/LIO-SAM)

5. LeGO-LOAM: Lightweight and Ground-Optimized Lidar Odometry and Mapping on Variable Terrain (LeGO-LOAM:轻型和地面优化的激光雷达测程和在不同的地形建图)\
[Paper](https://ieeexplore.ieee.org/document/8594299) | [Code](https://github.com/RobustFieldAutonomyLab/LeGO-LOAM)

6. Online Temporal Calibration for Monocular Visual-Inertial Systems (单目视觉惯性系统的在线时间标定)\
[Paper](https://arxiv.org/abs/1808.00692)



### SLAM：同时定位和建图
1. VINS-Mono: A Robust and Versatile Monocular Visual-Inertial State Estimator（VINS-Mono：一种鲁棒且通用的单目视觉惯性状态估计器）\
[Paper](https://ieeexplore.ieee.org/document/8421746) | [Code](https://github.com/HKUST-Aerial-Robotics/VINS-Mono)

2. ORB-SLAM3: An Accurate Open-Source Library for Visual, Visual-Inertial and Multi-Map SLAM（ORB-SLAM3:一个精确的可视化、视觉惯性和多地图SLAM的开源库）\
[Paper](https://arxiv.org/pdf/2007.11898.pdf) | [Code](https://github.com/UZ-SLAMLab/ORB_SLAM3)

3. LIC-Fusion2.0: LiDAR-Inertial-Camera Odometry with Sliding-Window Plane-Feature Tracking (LIC-Fusion2.0:滑动窗口平面特征跟踪的激光惯性相机测程法)\
[Paper](https://arxiv.org/pdf/2008.07196.pdf)

4. LIO-SAM: Tightly-coupled Lidar Inertial Odometry via Smoothing and Mapping (LIO-SAM:通过平滑和映射的紧耦合激光雷达惯性里程测量)\
[Paper](https://arxiv.org/abs/2007.00258) | [Code](https://github.com/TixiaoShan/LIO-SAM)

5. LeGO-LOAM: Lightweight and Ground-Optimized Lidar Odometry and Mapping on Variable Terrain (LeGO-LOAM:轻型和地面优化的激光雷达测程和在不同的地形建图)\
[Paper](https://ieeexplore.ieee.org/document/8594299) | [Code](https://github.com/RobustFieldAutonomyLab/LeGO-LOAM)

6. On-Manifold Preintegration for Real-Time Visual-Inertial Odometry (用于实时视觉惯性里程测量的流形预积分) \
[Paper](https://arxiv.org/abs/1512.02363)

7. 《视觉SLAM十四讲》高翔\
[Code](https://github.com/gaoxiang12/slambook2)



### 嵌入式开发：无人机平台开发
1. GAAS (Generalized Autonomy Aviation System) \
[website](https://github.com/generalized-intelligence/GAAS)

2. Quadrotor fast flight in complex unknown environments \
[Code](https://github.com/HKUST-Aerial-Robotics/Fast-Planner)

3. Code for autonomous drone race is now available on GitHub\
[Code](https://github.com/HKUST-Aerial-Robotics/Teach-Repeat-Replan)

4. ROS入门21讲 古月居\
[website](https://www.bilibili.com/video/BV1zt411G7Vn?from=search&seid=12656941446071723601)